-- Create table
create table CASES
(
  PID           VARCHAR2(255),
  BINAUTHDATE   TIMESTAMP(6),
  AUTHNAME      VARCHAR2(255),
  RECORDID      VARCHAR2(255),
  RegionOfBirth VARCHAR2(255),
  Name          VARCHAR2(255),
  Gender        VARCHAR2(10),
  AUTHSIG       VARCHAR2(255),
  SERIAL        INTEGER,
  GALLERY       VARCHAR2(255),
  AUTHDATE      TIMESTAMP(6),
  YearOfBirth   INTEGER,
  ISSUER        VARCHAR2(255),
  CASEID        VARCHAR2(255) not null,
  IDNO          VARCHAR2(30),
  PTYPE         VARCHAR2(5) default 1
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Add comments to the columns 
comment on column CASES.PTYPE
  is '1为全国在逃人员；2为本地关注；3测试；4其他';
-- Create/Recreate primary, unique and foreign key constraints 
alter table CASES
  add primary key (CASEID)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
alter table CASES
  add constraint U_CASES_RID unique (RECORDID)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Create/Recreate indexes 
create index I_CASES_PID on CASES (PID)
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
create index I_CASES_SERIAL on CASES (ISSUER, SERIAL)
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
