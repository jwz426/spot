package com.claude.spot.domain;

import junit.framework.Assert;
import org.junit.Test;

/**
 * Created by claudej on 1/22/2016.
 */
public class CaseInfoTest {

    @Test
    public void testGetDayOfBirth(){
        CaseInfo caseInfo = new CaseInfo();
        caseInfo.setIdCard("350581198209151233");

        Assert.assertEquals(caseInfo.getDayOfBirth(), "1982-09-15");
    }
}
