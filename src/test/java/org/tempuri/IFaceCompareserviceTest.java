/**
 * IFaceCompareserviceTest.java
 * <p/>
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.3  Built on : Jun 27, 2015 (11:17:49 BST)
 */
package org.tempuri;


import junit.framework.TestCase;
import org.apache.axis2.transport.http.HTTPConstants;

/*
 *  IFaceCompareserviceTest Junit test case
 */
public class IFaceCompareserviceTest extends TestCase {
    /**
     * Auto generated test method
     */
    public void testfcAddTask() throws Exception {
        IFaceCompareserviceStub stub = new IFaceCompareserviceStub("http://107.191.53.203:12306/ws/PWsFaceCompare.dll/soap/IFaceCompare"); //the default implementation should point to the right endpoint
        stub._getServiceClient().getOptions().setProperty(HTTPConstants.CHUNKED, false);
        IFaceCompareserviceStub.CompareFace compareFaceRequest = (IFaceCompareserviceStub.CompareFace) getTestObject(IFaceCompareserviceStub.CompareFace.class);
        compareFaceRequest.setImgBase64("xxxxxxxxx");
        compareFaceRequest.setRescount(10);
        compareFaceRequest.setThreshold(1);
        compareFaceRequest.setParam("Gender=Female,Male&YearOfBirth=1930-1939,1940-1949,1950-1959,1960-1969,1970-1979,1980-1989,1990-1999,2000-2009,<1930,>2010&RegionOfBirth=Europe,Australia,Asia,America,Africa&PTYPE=1,2,3,4,5");
        IFaceCompareserviceStub.CompareFaceResponse response = stub.compareFace(compareFaceRequest);
        System.out.println(response.get_return());
        assertNotNull(response);
    }

    //Create an ADBBean and provide it as the test object
    public org.apache.axis2.databinding.ADBBean getTestObject(
        Class type) throws Exception {
        return (org.apache.axis2.databinding.ADBBean) type.newInstance();
    }
}
