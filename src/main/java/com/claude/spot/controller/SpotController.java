package com.claude.spot.controller;

import com.claude.spot.domain.CompareConditions;
import com.claude.spot.domain.FaceCompareResult;
import com.claude.spot.service.ImageService;
import com.claude.spot.service.SpotService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import java.util.Base64;

@Controller
public class SpotController {

    @Autowired
    private SpotService spotService;

    @Autowired
    private ImageService imageService;

    @RequestMapping("/index.html")
    public String index(Model model) {
        return "index";
    }

    @RequestMapping(value = "/face-compare.html", method = RequestMethod.GET)
    public String getFaceCompare(Model model) {
        CompareConditions compareConditions = spotService.getCompareConditions();
        model.addAttribute("conditions", compareConditions);
        return "face-compare";
    }

    @RequestMapping(value = "/notice-info.html", method = RequestMethod.GET)
    public String noticeInfo(Model model) {
        return "notice-info";
    }

    @RequestMapping(value = "/photo/{id}", method = RequestMethod.GET, produces = "image/jpg")
    @ResponseBody
    public byte[] image(@PathVariable String id) {
        return imageService.getImageById(id).getImage();
    }

    @RequestMapping(value = "/cmpPhoto/{cmpId}", method = RequestMethod.GET, produces = "image/jpg")
    @ResponseBody
    public byte[] cmpImage(@PathVariable String cmpId){
        return imageService.getImageByCmpId(cmpId).getImage();
    }

}
