package com.claude.spot.controller;

import com.claude.spot.domain.ErrorMessage;
import com.claude.spot.domain.FaceCompareResult;
import com.claude.spot.domain.NoticeInfo;
import com.claude.spot.service.NoticeInfoService;
import com.claude.spot.service.SpotService;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.Base64;

@RestController
@RequestMapping("api")
public class ApiController {

    @Autowired
    private SpotService spotService;

    @Autowired
    private NoticeInfoService noticeInfoService;

    @RequestMapping(value = "/compare.json", method = RequestMethod.POST)
    public ResponseEntity<?> compare(int threshold, int count, String gender, String yearOfBirth, String pType, MultipartFile imageFile) {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.TEXT_HTML);
        if (imageFile != null && !imageFile.isEmpty()) {
            try {
                byte[] imageBytes = imageFile.getBytes();
                String imageBase64 = Base64.getEncoder().encodeToString(imageBytes);
                FaceCompareResult faceCompareResult = spotService.compare(threshold / 100f, imageBase64, count, gender, yearOfBirth, pType);
                spotService.getCaseInfos(faceCompareResult);
                return new ResponseEntity<>(faceCompareResult, httpHeaders, HttpStatus.OK);
            } catch (Exception e) {
                return new ResponseEntity<>(new ErrorMessage("比对失败:" + e.getMessage()), httpHeaders, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } else {
            return new ResponseEntity<>(new ErrorMessage("没有提交照片信息"), httpHeaders, HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "/noticeInfo/{cmpId}.json", method = RequestMethod.GET)
    public NoticeInfo getNoticeInfo(@PathVariable String cmpId) {
        Preconditions.checkArgument(!Strings.isNullOrEmpty(cmpId), "cmpId不能为空!");
        return noticeInfoService.getNoticeInfo(cmpId);
    }

}
