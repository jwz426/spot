package com.claude.spot.dao;

import com.claude.spot.domain.Facebase;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FacebaseDao {
    List<Facebase> getFacebases();
}
