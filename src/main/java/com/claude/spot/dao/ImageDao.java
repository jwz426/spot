package com.claude.spot.dao;

import com.claude.spot.domain.Image;

public interface ImageDao {
    Image getImageById(String id);

    Image getImageByCmpId(String id);
}
