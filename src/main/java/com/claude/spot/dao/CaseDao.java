package com.claude.spot.dao;

import com.claude.spot.domain.CaseInfo;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CaseDao {
    CaseInfo getCaseInfo(String caseId);
}
