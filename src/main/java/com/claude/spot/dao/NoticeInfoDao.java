package com.claude.spot.dao;

import com.claude.spot.domain.Image;
import com.claude.spot.domain.NoticeInfo;
import com.claude.spot.domain.SuspectCompareResult;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface NoticeInfoDao {

    NoticeInfo getNoticeInfoByCmpId(String cmpId);

    List<SuspectCompareResult> getCompareResult(String cmpId);
}
