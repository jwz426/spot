package com.claude.spot.domain;

import com.google.common.collect.ImmutableList;

import java.util.List;
import java.util.Map;

/**
 * Created by claudej on 1/5/2016.
 */
public class CompareConditions {

    private Threshold threshold;

    private List<Integer> counts;

    private Map<String, String> faceDbs;

    private List<String> yearOfBirths;

    public CompareConditions() {
        yearOfBirths = ImmutableList.of("1930-1939", "1940-1949", "1950-1959", "1960-1969", "1970-1979", "1980-1989", "1990-1999", "2000-2009", "<1930", ">2010");
    }

    public Threshold getThreshold() {
        return threshold;
    }

    public void setThreshold(Threshold threshold) {
        this.threshold = threshold;
    }

    public List<Integer> getCounts() {
        return counts;
    }

    public void setCounts(List<Integer> counts) {
        this.counts = counts;
    }

    public Map<String, String> getFaceDbs() {
        return faceDbs;
    }

    public void setFaceDbs(Map<String, String> faceDbs) {
        this.faceDbs = faceDbs;
    }

    public List<String> getYearOfBirths() {
        return yearOfBirths;
    }

    public void setYearOfBirths(List<String> yearOfBirths) {
        this.yearOfBirths = yearOfBirths;
    }
}
