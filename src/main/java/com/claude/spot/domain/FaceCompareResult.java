package com.claude.spot.domain;

import java.util.ArrayList;
import java.util.List;

public class FaceCompareResult {
    private int count;

    private List<MatchResult> matches = new ArrayList<MatchResult>();

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public List<MatchResult> getMatches() {
        return matches;
    }

    public void setMatches(List<MatchResult> matches) {
        this.matches = matches;
    }
}
