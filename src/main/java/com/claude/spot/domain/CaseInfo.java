package com.claude.spot.domain;

import com.google.common.base.Strings;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class CaseInfo implements Serializable {

    private String id;

    private String name;

    private String idCard;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    public String getDayOfBirth() {
        if (!Strings.isNullOrEmpty(idCard) && idCard.length() >= 15) {
            String dayOfBirthStr = idCard.substring(6, 14);
            LocalDate date = LocalDate.parse(dayOfBirthStr, DateTimeFormatter.BASIC_ISO_DATE);
            return date.format(DateTimeFormatter.ISO_LOCAL_DATE);
        }
        return null;
    }

}
