package com.claude.spot.domain;

import java.util.Date;
import java.util.List;

public class NoticeInfo {

    private String id;

    private Date createTime;

    private String fromDev;

    private String devAddress;

    private List<SuspectCompareResult> suspectCompareResults;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getFromDev() {
        return fromDev;
    }

    public void setFromDev(String fromDev) {
        this.fromDev = fromDev;
    }

    public String getDevAddress() {
        return devAddress;
    }

    public void setDevAddress(String devAddress) {
        this.devAddress = devAddress;
    }

    public List<SuspectCompareResult> getSuspectCompareResults() {
        return suspectCompareResults;
    }

    public void setSuspectCompareResults(List<SuspectCompareResult> suspectCompareResults) {
        this.suspectCompareResults = suspectCompareResults;
    }
}
