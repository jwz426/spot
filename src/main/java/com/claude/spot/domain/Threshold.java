package com.claude.spot.domain;

/**
 * Created by claudej on 1/5/2016.
 */
public class Threshold {

    private int min;

    private int max;

    public int getMin() {
        return min;
    }

    public void setMin(int min) {
        this.min = min;
    }

    public int getMax() {
        return max;
    }

    public void setMax(int max) {
        this.max = max;
    }
}
