package com.claude.spot.domain;

/**
 * Created by Administrator on 2016/1/13.
 */
public class SuspectCompareResult {

    private String caseId;

    private float score;

    private CaseInfo caseInfo;

    public String getCaseId() {
        return caseId;
    }

    public void setCaseId(String caseId) {
        this.caseId = caseId;
    }

    public float getScore() {
        return score;
    }

    public void setScore(float score) {
        this.score = score;
    }

    public CaseInfo getCaseInfo() {
        return caseInfo;
    }

    public void setCaseInfo(CaseInfo caseInfo) {
        this.caseInfo = caseInfo;
    }
}
