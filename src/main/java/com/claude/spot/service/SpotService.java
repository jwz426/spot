package com.claude.spot.service;

import com.alibaba.fastjson.JSON;
import com.claude.spot.dao.CaseDao;
import com.claude.spot.dao.FacebaseDao;
import com.claude.spot.domain.*;
import com.google.common.base.Charsets;
import com.google.common.base.Joiner;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableList;
import com.google.common.io.Resources;
import org.apache.axis2.transport.http.HTTPConstants;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.tempuri.IFaceCompareserviceStub;

import java.io.IOException;
import java.io.StringReader;
import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class SpotService {

    @Autowired
    private CaseDao caseDao;

    @Autowired
    private FacebaseDao facebaseDao;

    @Value("${facecompare.ws.url}")
    private String facecompareWsUrl;

    public CompareConditions getCompareConditions() {
        String conditionsJson = null;
        try {
            conditionsJson = Resources.toString(Resources.getResource("conditions.json"), Charsets.UTF_8);
        } catch (IOException e) {
            throw new IllegalStateException("Load conditions.json failed", e);
        }
        CompareConditions compareConditions = JSON.parseObject(conditionsJson, CompareConditions.class);

        compareConditions.setFaceDbs(getFacebases());

        return compareConditions;
    }

    private Map<String, String> getFacebases() {
        List<Facebase> facebases = facebaseDao.getFacebases();
        Map<String, String> facebaseMap = new HashMap<>();
        for (Facebase facebase : facebases) {
            facebaseMap.put(facebase.getId(), facebase.getName());
        }
        return facebaseMap;
    }

    public FaceCompareResult compare(float threshold, String image, int resource, String gender, String yearOfBirth, String pType) throws RemoteException, IllegalAccessException, InstantiationException, DocumentException {
        String faceCompareResultXml = callFaceCompareWS(threshold, image, resource, gender, yearOfBirth, pType);
        return processResultXml(faceCompareResultXml);
    }

    private String callFaceCompareWS(float threshold, String image, int resource, String gender, String yearOfBirth, String pType) throws InstantiationException, IllegalAccessException, RemoteException {
        IFaceCompareserviceStub stub = new IFaceCompareserviceStub(facecompareWsUrl);
        stub._getServiceClient().getOptions().setProperty(HTTPConstants.CHUNKED, false);
        IFaceCompareserviceStub.CompareFace compareFaceRequest = IFaceCompareserviceStub.CompareFace.class.newInstance();
        compareFaceRequest.setImgBase64(image);
        compareFaceRequest.setRescount(resource);
        compareFaceRequest.setThreshold(threshold);
        compareFaceRequest.setParam(createParam(gender, yearOfBirth, pType));
        IFaceCompareserviceStub.CompareFaceResponse response = stub.compareFace(compareFaceRequest);
        return response.get_return();
    }

    private String createParam(String gender, String yearOfBirth, String pType) {
        String genderParam = "Gender=Female,Male";
        if (!Strings.isNullOrEmpty(gender)) {
            genderParam = "Gender=" + gender;
        }
        String yearOfBirthParam = "YearOfBirth=1930-1939,1940-1949,1950-1959,1960-1969,1970-1979,1980-1989,1990-1999,2000-2009,<1930,>2010";
        if (!Strings.isNullOrEmpty(yearOfBirth)) {
            yearOfBirthParam = "YearOfBirth=" + yearOfBirth;
        }
        String pTypeParam = "PTYPE=" + pType;
        if (Strings.isNullOrEmpty(pType)) {
            List<String> allFacebases = facebaseDao.getFacebases().stream().map(Facebase::getId).collect(Collectors.toList());
            pTypeParam = "PTYPE=" + Joiner.on(",").join(allFacebases);
        }
        String regionOfBirthParam = "RegionOfBirth=Europe,Australia,Asia,America,Africa";
        return Joiner.on("&").join(ImmutableList.of(genderParam, yearOfBirthParam, regionOfBirthParam, pTypeParam));
    }

    private FaceCompareResult processResultXml(String faceCompareResultXml) throws DocumentException {
        FaceCompareResult faceCompareResult = new FaceCompareResult();

        SAXReader reader = new SAXReader();
        Document document = reader.read(new StringReader(faceCompareResultXml));
        System.out.println(faceCompareResultXml);
        Element transaction = document.getRootElement();
        Element result = transaction.element("Result");
        int count = Integer.valueOf(result.getText());

        if (count == 0) {
            Element matches = transaction.element("Matches");
            for (Iterator i = matches.elementIterator("Match"); i.hasNext(); ) {
                Element match = (Element) i.next();
                MatchResult matchResult = new MatchResult();
                matchResult.setId(match.attributeValue("id"));
                matchResult.setRank(Integer.valueOf(match.attributeValue("rank")));
                matchResult.setScore(Double.valueOf(match.attributeValue("score")) * 100);
                faceCompareResult.getMatches().add(matchResult);
            }
            faceCompareResult.setCount(faceCompareResult.getMatches().size());
        } else {
            throw new RuntimeException("调用比对接口失败!");
        }
        return faceCompareResult;
    }

    public void getCaseInfos(FaceCompareResult faceCompareResult) {
        for (MatchResult matchResult : faceCompareResult.getMatches()) {
            CaseInfo caseInfo = caseDao.getCaseInfo(matchResult.getId());
            matchResult.setCaseInfo(caseInfo);
        }
    }
}
