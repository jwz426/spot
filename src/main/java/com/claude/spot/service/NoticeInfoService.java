package com.claude.spot.service;


import com.claude.spot.dao.NoticeInfoDao;
import com.claude.spot.domain.NoticeInfo;
import com.claude.spot.domain.SuspectCompareResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@Service
public class NoticeInfoService {

    @Autowired
    private NoticeInfoDao noticeInfoDao;

    public NoticeInfo getNoticeInfo(@PathVariable String cmpId) {
        NoticeInfo noticeInfo = noticeInfoDao.getNoticeInfoByCmpId(cmpId);
        List<SuspectCompareResult> suspectCompareResults = noticeInfoDao.getCompareResult(cmpId);
        noticeInfo.setSuspectCompareResults(suspectCompareResults);
        return noticeInfo;
    }
}
