package com.claude.spot.service;


import com.claude.spot.dao.ImageDao;
import com.claude.spot.domain.Image;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ImageService {

    @Autowired
    private ImageDao imageDao;

    public Image getImageById(String id) {
        return imageDao.getImageById(id);
    }

    public Image getImageByCmpId(String cmpId){
        return imageDao.getImageByCmpId(cmpId);
    }
}
