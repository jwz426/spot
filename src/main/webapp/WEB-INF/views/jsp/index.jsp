<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>人像比对工具</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link class="skinstyle" rel="stylesheet" type="text/css" href="assets/theme/blue/layout.css" />
<script type="text/javascript" src="assets/js/jquery.min.js"></script>
<script type="text/javascript" src="assets/js/ui.js"></script>
<script type="text/javascript" src="assets/js/thirdParty/jquery.cookie.js"></script>
<script type="text/javascript" src="assets/js/thirdParty/underscore.js"></script>
<script type="text/javascript" src="assets/js/thirdParty/backbone.js"></script>
<script type="text/javascript">
    $(function(){
        var NavMenu = Backbone.View.extend({
            el: '#navMenu',
            events: {
                'click a': 'changeMenu'
            },
            changeMenu: function(e){
                var link = $(e.currentTarget).data('url');
                _.each(this.$el.find('li'), function(li){
                    $(li).removeClass('on');
                });
                $(e.currentTarget).parent('li').addClass('on');
                $('#mainFrame').attr('src', link);
            }
        });

        var navMenu = new NavMenu();
    });
</script>
</head>
<body>
<div id="container">
  <div id="header" class="header clearfix">
    <div class="logo"></div>
    <div class="header-right">
      <ul class="header-right-list">
         <li class="nowtime"><span id="nowTime">
        <label class="data"></label>
        <label class="week"></label>
        <label class="time"></label>
        </span> </li>
        <li><i class="header-icon i-user"></i>admin(系统用户)</a>
        </li>
        <li><span><a href="javascript:;" title="退出"><i class="header-icon i-logout"></i>退出</a></span> </li>
      </ul>
    </div>
  </div>
  <div id="sidebar">
    <div class="sidebar-list" id="navMenu">
      <div class="sidebar-tit">菜单</div>
      <ul>
         <li class="on"><a href="#" data-url="face-compare.html"><i class="sidebar-icon i-i10"></i><span>相似人员查找</span></a></li>
      </ul>
    </div>
  </div>
  <div id="main">
    <iframe id="mainFrame" name="mainFrame" scrolling="no" src="face-compare.html" width="100%" height="100%" frameborder="0"></iframe>
    <div id="footer">版权所有&nbsp;&nbsp;厦门市巨龙信息科技有限公司</div>
  </div>
</div>
</body>
</html>
