<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>人像比对工具</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link class="skinstyle" rel="stylesheet" type="text/css" href="assets/theme/blue/layout.css" />
        <script type="text/javascript" src="assets/js/jquery.min.js"></script>
        <script type="text/javascript" src="assets/js/ui.js"></script>
        <script type="text/javascript" src="assets/js/thirdParty/jquery.cookie.js"></script>
        <script type="text/javascript" src="assets/js/thirdParty/underscore.js"></script>
        <script type="text/javascript" src="assets/js/thirdParty/backbone.js"></script>
        <script type="text/javascript" src="assets/js/thirdParty/mustache.js"></script>
        <script type="text/javascript" src="assets/js/thirdParty/URI.js"></script>
        <script type="text/javascript" src="assets/js/thirdParty/jquery.blockUI.js"></script>
        <script type="text/javascript" src="assets/js/noticeInfo/NoticeInfoView.js"></script>
        <script type="text/javascript">
        $(function() {
            var cmpId = URI.parseQuery(window.location.search)['cmpId'];
            if (!cmpId) {
                alert("URL需要带上cmpId参数");
                return;
            }
            $.ajax({
                url: 'api/noticeInfo/' + cmpId + '.json',
                cache: false,
                dataType: 'json'
            }).done(function(noticeInfo) {
                for(var i = 0; i < noticeInfo.suspectCompareResults.length; i++){
                    noticeInfo.suspectCompareResults[i].scorePercent =
                        (noticeInfo.suspectCompareResults[i].score * 100).toFixed(2);
                }
                var noticeInfoView = new NoticeInfoView({
                    noticeInfo: noticeInfo
                });
                noticeInfoView.render();
            }).fail(function() {
                alert("获取警报信息失败!");
            });
        });
        </script>
        <script id="suspectTargetTemplate"  type="text/template">
        <div class="xy-tx">
                <img src="photo/{{ caseId }}"/>
        </div>
        <div class="xy-info">
                <ul>
                        <li>
                            姓名：<span class="ft-blue">{{ caseInfo.name }}</span>
                        </li>
                        <li>
                            身份证号：<span class="ft-blue">{{ caseInfo.idCard }}</span>
                        </li>
                        <li>
                            相似度：<span class="ft-blue">{{ scorePercent }}%</span>
                        </li>
                </ul>
        </div>
        </script>
        <script id="noticeInfoViewTemplate" type="text/template">
            <div class="col-12-2 col-height">
                    <div class="panel">
                        <div class="panel-header">
                                <h2>警报消息(<span class="ft-red">{{ suspectLength }}</span>)</h2>
                        </div>
                        <div class="panel-body" style="overflow-y:auto">
                                <ul class="bd-items tx-items">
                                        {{#suspectCompareResults}}
                                        <li data-caseid="{{ caseId }}" class="suspect-item">
                                            <div class="tx">
                                                    <img src="photo/{{ caseId }}" width="80" height="120" />
                                            </div>
                                            <p class="name">{{ caseInfo.name }}</p>
                                            <div class="ft-orange">{{ scorePercent }}%</div>
                                        </li>
                                        {{/suspectCompareResults}}
                                </ul>
                        </div>
                    </div>
            </div>
            <div class="col-12-5 col-height">
                    <div class="panel">
                        <div class="panel-header">
                                <h2>嫌疑人</h2>
                        </div>
                        <div class="panel-body">
                                <div class="xy-tx">
                                        <img src="cmpPhoto/{{ id }}"/>
                                </div>
                                <div class="xy-info">
                                        <ul>
                                                <li>
                                                    时间：<span class="ft-blue">{{ createTime }}</span>
                                                </li>
                                                <li>
                                                    地点：<span class="ft-blue">{{ devAddress }}</span>
                                                </li>
                                        </ul>
                                </div>
                        </div>
                    </div>
            </div>
            <div class="col-12-5 col-height">
                    <div class="panel">
                        <div class="panel-header">
                                <h2>嫌疑目标人</h2>
                        </div>
                        <div class="panel-body" id="suspectTarget">
                        </div>
                    </div>
            </div>
        </script>
    </head>
    <body>
        <div id="header" class="header clearfix">
            <div class="logo"></div>
            <div class="header-right">
                <ul class="header-right-list">
                    <li class="nowtime"><span id="nowTime">
                        <label class="data"></label>
                        <label class="week"></label>
                        <label class="time"></label>
                    </span> </li>
                    <li><i class="header-icon i-user"></i>admin(系统用户)</a>
                    </li>
                    <li><span><a href="javascript:;" title="退出"><i class="header-icon i-logout"></i>退出</a></span> </li>
                </ul>
            </div>
        </div>
        <div id="mainWrap">
            <div class="main-cont">
                <div class="breadcrumb">当前位置：<a href="javascript:;">首页</a>&nbsp;&gt;&nbsp;<span class="cur">警报信息</span></div>
                <div class="clearfix" id="noticeInfoView">
                </div>
            </div>
        </div>
        <div id="footer">版权所有&nbsp;&nbsp;厦门市巨龙信息科技有限公司</div>
    </body>
</html>
