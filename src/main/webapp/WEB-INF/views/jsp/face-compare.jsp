<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>人像比对工具</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link class="skinstyle" rel="stylesheet" type="text/css" href="assets/theme/blue/layout.css" />
<link rel="stylesheet" type="text/css" href="assets/ion.rangeslider/css/ion.rangeSlider.css">
<link rel="stylesheet" type="text/css" href="assets/ion.rangeslider/css/ion.rangeSlider.skinModern.css">
<script type="text/javascript" src="assets/js/jquery.min.js"></script>
<script type="text/javascript" src="assets/js/thirdParty/jquery.form.js"></script>
<script type="text/javascript" src="assets/ion.rangeslider/js/ion-rangeSlider/ion.rangeSlider.min.js"></script>
<script type="text/javascript" src="assets/js/thirdParty/underscore.js"></script>
<script type="text/javascript" src="assets/js/thirdParty/backbone.js"></script>
<script type="text/javascript" src="assets/js/thirdParty/mustache.js"></script>
<script type="text/javascript" src="assets/js/thirdParty/jquery.blockUI.js"></script>

<script type="text/javascript" src="assets/js/faceCompare/FaceCompareForm.js"></script>
<script type="text/javascript" src="assets/js/faceCompare/CompareResultPanel.js"></script>
<script type="text/javascript" src="assets/js/faceCompare/PaginationPanel.js"></script>
<script type="text/javascript">
$(function() {
    var faceCompareForm = new FaceCompareForm({
      thresholdMin : ${conditions.threshold.min},
      thresholdMax : ${conditions.threshold.max}
    });

    var compareResultPanel = new CompareResultPanel();
    var paginationPanel = new PaginationPanel();

});
</script>
<script id="compareResultTemplate" type="text/template">
<ul class="bd-items clearfix">
{{#matches}}
  <li>
     <div class="tx">
       <img src="photo/{{ id }}" width="80" height="120" />
     </div>
     <p class="name">{{ caseInfo.name }}</p>
     <div class="ft-orange">{{ scorePercent }}%</div>
     <div class="detail-info">
          <i class="arrow"></i>
          <div>
              姓名：<label>{{ caseInfo.name }}</label>
          </div>
          <div>
              <span>证件号：</span><label>{{ caseInfo.idCard }}</label>
          </div>
          <div>
              电话：<label>135015213</label>
          </div>
          <div>
              地址：<label>福建省厦门市XXXXXX</label>
          </div>
          <div>
              备注：<label>涉稳人中员</label>
          </div>
     </div>
  </li>
{{/matches}}
</ul>
</script>
</head>
<body>
<div class="main-cont">
  <div class="breadcrumb">当前位置：<a href="javascript:;">首页</a>&nbsp;&gt;&nbsp;<span class="cur">相似人员查找</span></div>
  <div class="clearfix">
    <form name="faceCompareForm" id="faceCompareForm" method="POST" action="api/compare.json" enctype="multipart/form-data">
        <div class="col-6-1 col-height condition-form">
          <div class="panel">
            <div class="panel-header">
              <h2>比对条件</h2>
            </div>
            <div class="panel-body">
               <div class="bd-tx" id="imagePreview">
                 <img src="assets/theme/blue/images/tp/tx1.jpg" width="107" height="137"/>
               </div>
               <div class="upload">
                  <input type="file" id="imageFile" name="imageFile" class="upload-file" />
               </div>
               <div class="cs-box">
                  <h2>相识度</h2>
                  <input type="text" id="threshold" name="threshold" value="70" />
                  <h2>结果数</h2>
                  <span class="select">
                     <select name="count">
                        <c:forEach var="item" items="${conditions.counts}">
                        <option value="${item}">${item}</option>
                        </c:forEach>
                     </select>
                  </span>
                  <h2>性别</h2>
                  <span class="select">
                     <select name="gender">
                        <option value="">--</option>
                        <option value="Male">男</option>
                        <option value="Female">女</option>
                     </select>
                  </span>
                  <h2>出生年份</h2>
                  <span class="select">
                     <select name="yearOfBirth">
                        <option value="">--</option>
                        <c:forEach var="item" items="${conditions.yearOfBirths}">
                        <option value="${item}"><c:out value="${item}" escapeXml="true"/></option>
                        </c:forEach>
                     </select>
                  </span>
                  <input type="hidden" name="yearMonth"/>
                  <h2>人脸库</h2>
                  <span class="select">
                     <select name="pType">
                        <option value="">--</option>
                        <c:forEach var="item" items="${conditions.faceDbs}">
                        <option value="${item.key}">${item.value}</option>
                        </c:forEach>
                     </select>
                  </span>
                  <div class="btn-zome">
                     <a href="javascript:;" class="btn" id="submitBtn"><span>比对</span></a>
                  </div>
               </div>
            </div>
          </div>
        </div>
    </form>
    <div class="content-panel">
      <div class="panel" id="compareResultPanel">
        <div class="panel-header">
          <h2>比对结果</h2>
        </div>
        <div class="panel-body">
           <ul class="bd-items clearfix">
           </ul>
        </div>
        <div class="panel-bottom clearfix" id="paginationPanel">
          <div class="pager-le">
            <span>每页
             <select id="pageSize">
                <option value="15">15</option>
                <option value="20">20</option>
                <option value="50">50</option>
             </select>
            条</span>
            <span id="pageTotal">共0条</span>
          </div>
          <div class="pager-ri">
            <ul class="pager">
              <li class="first"><a id="firstPage" href="javascript:;">首页</a></li>
              <li class="prev"><a id="prevPage" href="javascript:;">上一页</a></li>
              <li class="curr"><span id="pageStatus">0</span></li>
              <li class="next"><a id="nextPage" href="javascript:;">下一页</a></li>
              <li class="last"><a id="lastPage" href="javascript:;">末页</a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</body>
</html>
