var NoticeInfoView = Backbone.View.extend({
    el: '#noticeInfoView',
    events: {
        'click .suspect-item': 'clickSuspect'
    },
    initialize: function(options) {
        this.noticeInfo = options.noticeInfo;
        this.suspectTargetTemplate = $('#suspectTargetTemplate').html();
        Mustache.parse(this.suspectTargetTemplate);
    },
    render: function() {
        var noticeInfoViewTemplate = $('#noticeInfoViewTemplate').html();
        Mustache.parse(noticeInfoViewTemplate);
        this.$el.html(Mustache.render(noticeInfoViewTemplate, _.extend({
                suspectLength: function(){
                    return this.suspectCompareResults.length;
                }
            },
            this.noticeInfo
        )));
        this.renderSuspectTarget(this.noticeInfo.suspectCompareResults[0]);
    },
    renderSuspectTarget: function(suspect){
        this.$el.find('#suspectTarget').html(Mustache.render(this.suspectTargetTemplate, suspect));
    },
    clickSuspect: function(e){
        var caseId = $(e.currentTarget).data('caseid');
        var suspect = _.find(this.noticeInfo.suspectCompareResults, function(suspect){
            return suspect.caseId == caseId
        });
        this.renderSuspectTarget(suspect);
    }
});
