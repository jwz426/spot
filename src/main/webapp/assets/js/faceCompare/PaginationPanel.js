var PaginationModel = Backbone.Model.extend({

});

var PaginationPanel = Backbone.View.extend({
    el: '#paginationPanel',

    events: {
        'change #pageSize': 'changePageSize',
        'click #firstPage': 'clickFirstPage',
        'click #prevPage': 'clickPrevPage',
        'click #nextPage': 'clickNextPage',
        'click #lastPage': 'clickLastPage'
    },

    initialize: function(){
        this.listenTo(Backbone, 'data:compareResult', this.initPagination);
        this.model = new PaginationModel({
            page: 1,
            pageSize: 15
        });

        this.listenTo(this.model, 'change:page', this.changePage);
    },

    initPagination: function(compareResult){
        this.compareResult = compareResult;
        this.changeCompareResult(this.model, compareResult);
    },

    changeCompareResult: function(model, compareResult){
        var total = compareResult.count;
        var totalPage = parseInt(total / model.get('pageSize'));
        if(total % model.get('pageSize') > 0){
            totalPage = totalPage + 1;
        }
        this.$el.find('#pageTotal').html('共'+total+'条');
        this.model.set('totalPage', totalPage);
        this.model.set('page', 1);
        if(this.model.get('page') === 1){
            this.changePage(this.model, 1);
        }
    },

    changePage: function(model, page){
        this.$el.find('#pageStatus').html(page + '/' + model.get('totalPage'));
        Backbone.trigger('pagination:change', this.compareResult.matches.slice((page - 1) * this.model.get('pageSize'), page * this.model.get('pageSize')));
    },

    changePageSize: function(e){
        this.model.set({
            pageSize: $(e.currentTarget).val()
        });
        this.changeCompareResult(this.model, this.compareResult);
    },

    clickFirstPage: function(){
        if(this.model.get('page') !== 1){
            this.model.set('page', 1);
        }
    },

    clickPrevPage: function(){
        if(this.model.get('page') > 1){
            this.model.set('page', this.model.get('page') - 1);
        }
    },

    clickNextPage: function(){
        if(this.model.get('page') < this.model.get('totalPage')){
            this.model.set('page', this.model.get('page') + 1);
        }
    },

    clickLastPage: function(){
        if(this.model.get('page') !== this.model.get('totalPage')){
            this.model.set('page', this.model.get('totalPage'));
        }
    }
});
