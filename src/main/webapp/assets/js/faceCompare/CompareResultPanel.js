var CompareResultPanel = Backbone.View.extend({
    el: '#compareResultPanel',
    events: {
        'mouseover ul.bd-items li': 'showDetailInfo',
        'mouseout ul.bd-items li': 'hideDetailInfo'
    },
    initialize: function() {
        this.listenTo(Backbone, 'pagination:change', this.renderResult);
    },
    renderResult: function(compareResult) {
        var compareResultTemplate = $('#compareResultTemplate').html();
        Mustache.parse(compareResultTemplate);

        for(var i = 0; i < compareResult.length; i++){
            compareResult[i].scorePercent = compareResult[i].score.toFixed(2);
        }
        this.$el.find('.panel-body').html(Mustache.render(compareResultTemplate, {
            matches: compareResult
        }));
    },
    showDetailInfo: function(e) {
        var windowWidth = $(window).width();
        var positionRi = $(e.currentTarget).position().left;
        var target = $(e.currentTarget).find(".detail-info");
        var liWidth = $(e.currentTarget).width() + target.width();
        if (positionRi + liWidth > windowWidth) {
            target.addClass("posleft");
            target.find(".arrow").addClass("posleft");
        }
        $(e.currentTarget).find(".detail-info").show();
    },
    hideDetailInfo: function(e) {
        $(e.currentTarget).find(".detail-info").hide();
    }
});