var FaceCompareForm = Backbone.View.extend({
    el: '#faceCompareForm',
    events: {
        'change .upload-file': 'loadImageFile',
        'click #submitBtn': 'submitForm'
    },
    initialize: function(options){
        if(!window.FileReader && navigator.appName === "Microsoft Internet Explorer"){
            $('#imagePreview').html('<div id="imagePreviewFrame"></div>')
        }

        $("#threshold").ionRangeSlider({
            grid: true,
            min: options.thresholdMin,
            max: options.thresholdMax,
            from: 10,
            step: 1,
            prettify_enabled: false
        });

        $('#faceCompareForm').ajaxForm({
            dataType: 'json',
            beforeSubmit: function(arr, form, options){
                if($('#imageFile').val() === ''){
                    alert('请选择人员照片');
                    return false;
                }

                if($('select[name=year]').val() !== ''){
                    if($('select[name=month]').val() === ''){
                        alert('请选择出生月份');
                        return false;
                    }
                }

                if($('select[name=month]').val() !== ''){
                    if($('select[name=year]').val() === ''){
                        alert('请选择出生年份');
                        return false;
                    }
                }

                $.blockUI({
                    message: '正在比对中，请稍后...',
                    css: {
                        border: 'none',
                        padding: '15px',
                        backgroundColor: '#000',
                        '-webkit-border-radius': '10px',
                        '-moz-border-radius': '10px',
                        opacity: .5,
                        color: '#fff'
                    }
                });
            },
            success: _.bind(this.fetchCompareResult, this),
            error: _.bind(this.fetchCompareError, this),
            complete: function(){
                $.unblockUI();
            }
        });

    },
    loadImageFile: function(){
        if (window.FileReader) {
            var oFReader = new window.FileReader(),
            rFilter = /^(?:image\/bmp|image\/cis\-cod|image\/gif|image\/ief|image\/jpeg|image\/jpeg|image\/jpeg|image\/pipeg|image\/png|image\/svg\+xml|image\/tiff|image\/x\-cmu\-raster|image\/x\-cmx|image\/x\-icon|image\/x\-portable\-anymap|image\/x\-portable\-bitmap|image\/x\-portable\-graymap|image\/x\-portable\-pixmap|image\/x\-rgb|image\/x\-xbitmap|image\/x\-xpixmap|image\/x\-xwindowdump)$/i;

            oFReader.onload = function (oFREvent) {
                var imagePreview = $("#imagePreview").find('img');
                imagePreview.get(0).src = oFREvent.target.result;
            };

            var aFiles = document.getElementById("imageFile").files;
            if (aFiles.length === 0) { return; }
            if (!rFilter.test(aFiles[0].type)) { alert("You must select a valid image file!"); return; }
            oFReader.readAsDataURL(aFiles[0]);
        }
        if (navigator.appName === "Microsoft Internet Explorer") {
            $('#imageFile').select();
            var imageUrl = document.selection.createRange().text;
            document.getElementById("imagePreviewFrame").filters.item("DXImageTransform.Microsoft.AlphaImageLoader").src = imageUrl;
        }
    },
    submitForm: function(){
        $('#faceCompareForm').submit();
    },
    fetchCompareResult: function(compareResult){
        Backbone.trigger('data:compareResult', compareResult);
    },
    fetchCompareError: function(){
        alert("照片比对失败!");
    }
});
