/*iframe 高度计算*/
function SetframeHeight() {
	var header = $("#header"),
	frame = $("#mainFrame"),
	cont = $('#main'),
	sidebar = $('#sidebar'),
	footer = $('#footer');
	var headerHeight, footerHeight,bHeight, sHeight, height;
	headerHeight = header.height();
	footerHeight = footer.height();
	sHeight = document.documentElement.clientHeight;
	bHeight = frame.contents().find("body").height();
	if(sidebar){
		height = Math.max((bHeight)? bHeight:0, sHeight - headerHeight - footerHeight);
		frame.attr("height",height);
		cont.css("height",height + footerHeight + "px");
		sidebar.css("height",height + footerHeight + "px");
	}else{
		frame.attr("height",height);
		cont.css("height",sHeight - headerHeight - footerHeight + "px");
	}
}

/*=========================系统日期=========================*/
function data(){
	var d=new Date(),str='';
	str +=d.getFullYear()+'年'; //获取当前年份
	str +=d.getMonth()+1+'月'; //获取当前月份（0——11）
	str +=d.getDate()+'日';
	return str;
}
function week(){
	var arr = new Array("日", "一", "二", "三", "四", "五", "六");
    var week = new Date().getDay();
    return "星期" + arr[week];
}

function time(){
	var d=new Date();
	var hours=d.getHours();
    var minutes = d.getMinutes()>9?d.getMinutes().toString():'0' + d.getMinutes();
    var seconds = d.getSeconds()>9?d.getSeconds().toString():'0' + d.getSeconds();
    var str = hours + ':' + minutes + ':' + seconds;
    return str;
}

setInterval(function(){
	$("#nowTime").children(".data").html(data);
},1000);

$(window).bind('load',function(){
	SetframeHeight();

	$("#mainFrame").bind('load', function(){
		SetframeHeight();
	})

});

//屏幕resize
$(window).bind('resize',function(){
	SetframeHeight();
});
