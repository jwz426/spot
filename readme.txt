1、项目结构

\document       # 需求沟通文档
\lib            # 引用的第三方库
\software       # 用到的第三方软件
\src            # 项目源代码
build.gradle    # 项目构建文件

1.1 开发必装软件

JDK8
Idea

2、开发过程

2.1 Git
项目使用Git进行源代码管理，你们可以在公司内部搭一个git的服务器来host源代码，也可以使用第三方的git服务。比如github或者bitbucket。

2.2 IDE
建议使用Idea进行项目开发。

2.2.1 导入Idea
直接Idea的New -> File -> Project from existing sources，然后选择Gradle项目模型导入。

2.2.2 开发测试
在Idea里头运行gradle appStart可以运行起项目。
运行gradle appStartDebug可以运行包含Remote debug端口的项目。然后在Idea中启动一个remote debug来调试项目。

2.3 配置文件
项目只需要一个配置文件application.properties，里面配置数据源的参数以及WS的地址。
可以根据不同环境配置不同的配置文件。
/src/main/resources/application.properties

2.4 打包
使用./gradlew buildProduct可以把项目打包。打包完的项目在build/output/spot里头。

2.5 运行
进入build/output/spot目录，使用
start.bat  # 运行
stop.bat # 停止
start.bat --runnerArg=-DextPropPath=xxxx # 指定application.properties配置文件的路径。

3. 项目架构
      +---------------------------------------+
      |          BackboneJS + Mustache        |
      +---------------------------------------+

      +---------------------------------------+

      +---------------------------------------+
      |              Spring MVC               |
      +---------------------------------------+

      +------------------+ +------------------+
      |   MyBatis        | |    Axis 2        |
      +------------------+ +------------------+

      +---------------------------------------+

      +------------------+  +-----------------+
      |   Oracle DB      |  |   Web Service   |
      +------------------+  +-----------------+
      
项目采用Restful API + Frontend MVC的架构。
Frontend MVC采用BackboneJS实现。
Restful API采用Spring MVC实现。
数据层访问采用Mybatis实现。
调用WS采用Axis2生成Client调用。

3.1 测试地址

3.1.1. 比对
http://localhost:7070/spot/index.html

3.1.2. 警报信息
http://localhost:7070/spot/notice-info.html?cmpId=100000000002

spot.war\WEB-INF\classes
